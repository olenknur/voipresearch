package com.lte.ltevoip.api.utils;
public final class AuthenticationException extends RuntimeException {
    public AuthenticationException(String s) {
        super(s);
    }
}
