package com.lte.ltevoip;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.lte.ltevoip.api.ApiClient;
import com.lte.ltevoip.api.ApiInterface;
import com.lte.ltevoip.api.model.TokenResponse;
import com.lte.ltevoip.helper.GlobalVars;

import oracle.live.api.CommunicationFragment;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    protected static final String FRAGMENT_TAG = CommunicationFragment.class.getName();
    private CommunicationFragment mCxFragment;
    private String token="eyJ0eXAiOiJKV1QiLCJraWQiOiJ3VTNpZklJYUxPVUFSZVJCL0ZHNmVNMVAxUU09IiwiYWxnIjoiUlMyNTYifQ.eyJzdWIiOiJlMjEyMDkxNC0xZTMwLTQ0YTktODg0OS0xNDljMWRjNTJlYmEiLCJjdHMiOiJPQVVUSDJfR1JBTlRfU0VUIiwiYXV0aF9sZXZlbCI6MCwiYXVkaXRUcmFja2luZ0lkIjoiYWRmMTliZGQtYTRiMi00M2Y5LTlkYTMtNzI5YmQxZjA0NTNhLTM1ODg0MSIsImlzcyI6Imh0dHBzOi8vY2lhbWFtcHJlcGRhcHAuY2lhbS50ZWxrb21zZWwuY29tOjEwMDAzL29wZW5hbS9vYXV0aDIvdHNlbC9ob21lbHRlL3dlYiIsInRva2VuTmFtZSI6ImFjY2Vzc190b2tlbiIsInRva2VuX3R5cGUiOiJCZWFyZXIiLCJhdXRoR3JhbnRJZCI6ImhGS3BkWVUwSUJSVlJaQ1VlZFhCbDI5bU5kWS5LamFkaFJxUE5aNEwxaTFkSTJCbl9FVzByQmMiLCJub25jZSI6InRydWUiLCJhdWQiOiJyZzhhYmM0MzAzMjkxNjE5ajFrd3E0OTE3NzNibTAzNCIsIm5iZiI6MTU5MDIxMzI2MCwiZ3JhbnRfdHlwZSI6ImF1dGhvcml6YXRpb25fY29kZSIsInNjb3BlIjpbIm9wZW5pZCIsInByb2ZpbGUiXSwiYXV0aF90aW1lIjoxNTkwMjEzMjU1LCJyZWFsbSI6Ii90c2VsL2hvbWVsdGUvd2ViIiwiZXhwIjoxNTkwMjk5NjYwLCJpYXQiOjE1OTAyMTMyNjAsImV4cGlyZXNfaW4iOjg2NDAwLCJqdGkiOiJoRktwZFlVMElCUlZSWkNVZWRYQmwyOW1OZFkuSUItcHVrVS1yUlRseWlNWjU5dXlTR0Qwd2drIn0.CQoFzx4ekMI6xDORY902ByY4Gg6UHQHjAkj4N40HED62pKkJoO_Mb8iz9uJ89YWUslppfCyBkm0jf6981aBKduOC4nYr_RtA6OK2BKeBU6ri8Eus8qh6Knp6hsbpRxUotXVtMoB7h02d4m0VMkj61xcSOwweZUFQKjouZiP7SStxthK7Z-r8njS91RqsiusQ9h9ku7KsX8nygMiDZErAlMQEOD5wSyIRUjc_teq_jhIcpfkNeG7XiOy8ZMRxWZ6NM9_ItN3ZqGRqEftpwSJAct4FPn6y0Y-vxxYu5n7Mv0KMLfS-HxkVNSesYS6ouYWiWS3D07wnGkJqEzKoFQ2WLg";
    private String tokenZendesk="eyJ0eXAiOiJKV1QiLCJraWQiOiJ3VTNpZklJYUxPVUFSZVJCL0ZHNmVNMVAxUU09IiwiYWxnIjoiUlMyNTYifQ.eyJzdWIiOiJlMjEyMDkxNC0xZTMwLTQ0YTktODg0OS0xNDljMWRjNTJlYmEiLCJjdHMiOiJPQVVUSDJfR1JBTlRfU0VUIiwiYXV0aF9sZXZlbCI6MCwiYXVkaXRUcmFja2luZ0lkIjoiYWRmMTliZGQtYTRiMi00M2Y5LTlkYTMtNzI5YmQxZjA0NTNhLTcxNzE2MCIsImlzcyI6Imh0dHBzOi8vY2lhbWFtcHJlcGRhcHAuY2lhbS50ZWxrb21zZWwuY29tOjEwMDAzL29wZW5hbS9vYXV0aDIvdHNlbC9ob21lbHRlL3dlYiIsInRva2VuTmFtZSI6ImFjY2Vzc190b2tlbiIsInRva2VuX3R5cGUiOiJCZWFyZXIiLCJhdXRoR3JhbnRJZCI6ImhGS3BkWVUwSUJSVlJaQ1VlZFhCbDI5bU5kWS5NalRGaFVfQnd3cnk4UkRtUUxnemFzYlMtODAiLCJub25jZSI6InRydWUiLCJhdWQiOiJyZzhhYmM0MzAzMjkxNjE5ajFrd3E0OTE3NzNibTAzNCIsIm5iZiI6MTU5MDY0Njg4OCwiZ3JhbnRfdHlwZSI6ImF1dGhvcml6YXRpb25fY29kZSIsInNjb3BlIjpbIm9wZW5pZCIsInByb2ZpbGUiXSwiYXV0aF90aW1lIjoxNTkwNjQ2ODc1LCJyZWFsbSI6Ii90c2VsL2hvbWVsdGUvd2ViIiwiZXhwIjoxNTkwNzMzMjg4LCJpYXQiOjE1OTA2NDY4ODgsImV4cGlyZXNfaW4iOjg2NDAwLCJqdGkiOiJoRktwZFlVMElCUlZSWkNVZWRYQmwyOW1OZFkuVWk3YWw1VTlUWnMtRjRFOFZjSXF4ZVNGSDkwIn0.XILpkDGv3beAsfhbpYUs_LnkWF5zLLAKqZD-P5jHTpjE-hy5-Id71agCBxUqq79pOplBuWGAanHFGOXr8EjaNp5AnPSCcDvd-ZmWjIxqAT0TRJbY72LsyooBHPIPZp2JBGHZqIYrex49iTEjlh5CavgSjFg_uxoTo51EIgibzOBh5fKagghf_rDqPgjI2lfr_E0lUWvFnyGHcJP7d1mtNXU9fIX9H_uItoQ6wzx71SgUW7EPyEjahjJJcAhHf7_08eQi63HhH5rzanPa9U-26-QX5hxYGpbJjw_mmX305zYH8zfD-jsdAGA8X5RNBnaPf91_YXQh75JQLGgP5mLsTQ";
    private TextView tvResponse;
    private ApiInterface service;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        service = ApiClient.getApiService(this);
        tvResponse= findViewById(R.id.tv_response);
//        getToken();
        CommunicationFragment.settings.startVideoInFullScreen = true;
        CommunicationFragment.settings.startVideoWithFrontCamera = true;

        // Specify the settings for component behavior.
        CommunicationFragment.service.setAddress(GlobalVars.lxURL);
        CommunicationFragment.service.setTenantID(GlobalVars.tenant);
        CommunicationFragment.service.setUserID(GlobalVars.username);
        CommunicationFragment.service.setClientID(GlobalVars.clientID);

        // Optional context attributes...
        CommunicationFragment.contextAttributes.set("fullName","Bob Dobbs");
        CommunicationFragment.contextAttributes.set("email","customer@example.com");
        CommunicationFragment.contextAttributes.set("phone","415-555-1212");
        CommunicationFragment.contextAttributes.set("location","Madrid");

        // Required context attributes...
        CommunicationFragment.contextAttributes.set("appLocation",GlobalVars.appLocation);
        CommunicationFragment.service.setAuthToken(tokenZendesk);

//        initCxFragment();
        createLogin();
    }

    private void getToken(){
    Call<TokenResponse> call = service.fetchToken(token);
    call.enqueue(new Callback<TokenResponse>() {
            @Override
            public void onResponse(Call<TokenResponse> call, Response<TokenResponse> response) {
                tvResponse.setText(response.body().getStatus()+" , \n"+response.body().getCode());
                if (response.body().getStatus()) {
                    tokenZendesk = response.body().getTokenData().getAccessToken();
                    Toast.makeText(MainActivity.this, "Success", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<TokenResponse> call, Throwable t) {
                Log.e("ErrorToken",t.getLocalizedMessage());
            }
        });
    }


    private void initCxFragment() {
        final FragmentManager manager = getSupportFragmentManager();
        mCxFragment = (CommunicationFragment) manager.findFragmentByTag(FRAGMENT_TAG);

        if (mCxFragment == null) {
            mCxFragment = CommunicationFragment.newInstance();

            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.cx_container, mCxFragment, FRAGMENT_TAG)
                    .commit();
        }
    }

    private Runnable createLogin() {
        return new Runnable() {
            @Override
            public void run() {
                initCxFragment();
            }
        };
    }

}
